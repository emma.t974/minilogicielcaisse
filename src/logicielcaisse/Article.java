/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logicielcaisse;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author dieu
 */
public class Article extends BDD{
    
    // private Map <String,String> price = new HashMap<>();
    private int price []= new int[2];
    
    public Article(){
      
    }
    
    //On récupère la liste des prix au chargement du logiciel
 public void getPriceAll() throws SQLException{
     ResultSet resultset = this.request().executeQuery("SELECT price FROM categorie order by id desc");
    //ResultSetMetaData resultMeta = resultset.getMetaData();
     int i = 0;
    
        while(resultset.next()){
            price[i] = resultset.getInt("price");
            //System.out.println(resultset.getInt("price"));
            i++;
        }

 }
    
  public float getPrice (int id) throws SQLException{  
        
        float p = 0f;
        /*ResultSet resultset = this.request().executeQuery("SELECT * FROM categorie WHERE id="+id);
        //System.out.println(resultset);
        if(resultset.next()){
            return p = resultset.getFloat("price"); 
        }
        else{
            return p;
        }*/
     
       for(int i=0;i<price.length;i++){
           //System.out.println(price[i]);
           if(price.length - i == id){
               return p = price[i];
           }
       }
       
       return p;
       
  }
  
  public int DernierCommandeAjout() throws SQLException{
      ResultSet result = this.request().executeQuery("SELECT id FROM commande ORDER BY id desc LIMIT 1");
      result.next();
      
      return result.getInt("id");
  }
  
  public void ValideCommande(ArrayList lesArticles, ArrayList lesPrix,Vendeur unVendeur) throws SQLException{
      //On crée la commande;
      this.request().executeUpdate("INSERT INTO commande (date,heure,idVendeur) values(curdate(),curtime(),"+unVendeur.getId()+")");
      
      //On ajoute la contenu de la commande;
      for(int i =0;i<lesArticles.size();i++){
          this.request().executeUpdate("INSERT INTO contenu_commande (idCommande,Article,Price) values ("+DernierCommandeAjout()+",'"+lesArticles.get(i)+"',"+lesPrix.get(i)+")");
         
      }
      
  }
  
  public HashMap getCommandePlusDemander(String Cat) throws SQLException{
      // On crée le tableau
      
      HashMap <String,Integer> tab = new HashMap<>();
      tab.clear();
      //On stock le résultat dans une notre ArrayList
      ResultSet result = this.request().executeQuery("SELECT Article, count(Article) as \"NbArticle\" from contenu_commande where Article like \""+Cat+" %\" group by article order by NbArticle desc;");
      
      //On ajoute les résultat dans notre tableau
      while(result.next()){
          tab.put(result.getString("Article"),result.getInt("NbArticle"));
      }
      
      return tab;
  }
  
}
