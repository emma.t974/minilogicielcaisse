/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logicielcaisse;

/**
 *
 * @author EMMA TONY
 */

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BDD {
   
    //Base de donnée externe 
    
    //private String host = "mysql-emmat974.alwaysdata.net";
   // private String user = "emmat974_caisse";
   // private String password = "25rZa9YwK";
  //  private String bdd = "emmat974_caisse";
    
    
    //Test en local
    
    private String host = "localhost";
    private String user = "root";
    private String password = "";
    private String bdd = "caisse";
    
    
    private int port = 3306;
    
    
    
    public BDD(){
        
    }
    public BDD(String h,String u, String pwd,String b,int p){
        this.host = h;
        this.user = u;
        this.password = pwd;
        this.port = p;
        this.bdd = b;
    }
    
    public void connexion() throws SQLException{
        
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println("Le pilote JDBC MySQL a été chargé");
            DriverManager.getConnection("jdbc:mysql://"+this.host+":"+this.port+"/"+this.bdd, this.user, this.password);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BDD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Statement request() throws SQLException{
        Statement state;
        Connection conn = DriverManager.getConnection("jdbc:mysql://"+this.host+":"+this.port+"/"+this.bdd, this.user, this.password);
        state = conn.createStatement();
            
            return state;
  
    }
}
