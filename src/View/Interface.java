/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.math.RoundingMode;
import java.net.URI;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.RowFilter.Entry;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import static jdk.nashorn.internal.objects.NativeArray.map;
import logicielcaisse.Article;
import logicielcaisse.Vendeur;
/**
 *
 * @author EMMA TONY
 */
public class Interface extends javax.swing.JFrame {

    Vendeur session;
    Article lesArticles = new Article();
    String sDescription = "";
    ArrayList lesArticle = new ArrayList();
    ArrayList lesPrix = new ArrayList();
   
    float price;
    
    /**
     * Creates new form Interface
     */
    public Interface() {
       

        try {
            initComponents();
            pannelCnx(true);
            setIcon();
            dateCourante();
            disableBtn();
            cancelUneCmd.setEnabled(false);
            this.setResizable(false);
            lesArticles.getPriceAll();
            TopArticle.setVisible(false);
        } catch (SQLException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
 
    }

    private void pannelCnx(boolean i){
        if(i == true){
            Connexion.setVisible(true);
            Caisse.setVisible(false);
            
            gestionCompte.setVisible(false);
           
        }
        else if(i == false){
            Connexion.setVisible(false);
            Caisse.setVisible(true);
            sessionopen.setText(session.getVendeur());
            gestionCompte.setVisible(true);
        }
        else{
             JOptionPane.showMessageDialog(this, "Une erreur s'est produit lors du lancement du logiciel");
             System.exit(0);
        }
    }
    
    private void setIcon(){
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("iconframe.png")));
    }
    
    private void dateCourante(){
        new Timer(0, new ActionListener(){
            //@Override
            public void actionPerformed(ActionEvent e){
                Date d = new Date();
                SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat h = new SimpleDateFormat("HH:mm:ss");
                heures.setText(h.format(d));
                date.setText(dt.format(d));
            }
        }).start();
        

    }
    
    private void disableBtn(){
        
        //Bouton commande
        chocolat.setEnabled(false);
        poire.setEnabled(false);
        ananas.setEnabled(false);
        fraise.setEnabled(false);
        cafe.setEnabled(false);
        vanille.setEnabled(false);
        rhum.setEnabled(false);
        framboise.setEnabled(false);
        
        //Bouton Action Commande
        cancelCmd.setEnabled(false);
        validCmd.setEnabled(false);
        
        
        //Bouton Action ValideCommande
        cancel.setEnabled(false);
        valid.setEnabled(false);
        
        //Re-Active les boutons Glace/MilkShake
        glace.setEnabled(true);
        milkshake.setEnabled(true);
    }
    
    //Désactive les boutons Glace/Milk
    private void disableChoose(){
        glace.setEnabled(false);
        milkshake.setEnabled(false);
    }
    //Désactive les boutons Cmd
    private void disableCmd(){
              //Bouton commande
        chocolat.setEnabled(false);
        poire.setEnabled(false);
        ananas.setEnabled(false);
        fraise.setEnabled(false);
        cafe.setEnabled(false);
        vanille.setEnabled(false);
        rhum.setEnabled(false);
        framboise.setEnabled(false);
    }
    
    //Activer les boutons Cmd
    private void enabledCmd(){
       //Bouton commande
        chocolat.setEnabled(true);
        poire.setEnabled(true);
        ananas.setEnabled(true);
        fraise.setEnabled(true);
        cafe.setEnabled(true);
        vanille.setEnabled(true);
        rhum.setEnabled(true);
        framboise.setEnabled(true);
    }
    
    //Activer les bouton ActionCmd
    
    private void enabledACmd(){
        cancelCmd.setEnabled(true);
        validCmd.setEnabled(true);
    }
    
    //Activer les boutons ActionCmdValid
    
    private void enabledAVCmd(){
        cancel.setEnabled(true);
        valid.setEnabled(true);
    }
    
    // Affichage la description
    
    private void description(String e){
        sDescription += e+ " ";
        description.setText(sDescription);
        prix.setText(String.valueOf(price));
        
    }
    
    //On vide la description
    private void resetDescription(){
        sDescription = "";
        price = 0f;
        description.setText(sDescription);
        prix.setText(String.valueOf(price));
    }
    
    //Ajout un element dans le tableau
    private void add(){
        DefaultTableModel model = (DefaultTableModel) recap.getModel();
        
        model.addRow(new Object[]{sDescription, price});
        lesArticle.add(sDescription);
        lesPrix.add(price);
    }
    
    //Enlever un element dans le tableau
    private void delete(){
        DefaultTableModel model = (DefaultTableModel) recap.getModel();
       try{
            int SelectedRowIndex = recap.getSelectedRow();
            model.removeRow(SelectedRowIndex);
            lesArticle.remove(SelectedRowIndex);
            lesPrix.remove(SelectedRowIndex);
            Total();
            
       }catch(Exception ex){
           JOptionPane.showMessageDialog(null, "Vous devrez sélectionner un élément dans la liste");
           System.out.println(ex);
       }
       
       
    }
    
    //Maj tableau
    private void topCommande(HashMap Articles){
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        
      //System.out.println("Parcours de l'objet HashMap : ");
       System.out.println("Les articles sont : " + Articles.entrySet()); 
      
           // int SelectedRowIndex = table.getSelectedRow();
            table.removeAll();
    // ((DefaultTableModel)table.getModel()).removeRow(10);
       
       Articles.keySet().forEach((i) -> {
           //  System.out.println(i);
           //  System.out.println(Articles.get(i));
           
           model.addRow(new Object[]{i, Articles.get(i)});
        });
    }
    
    //CANCEL UNE COMMANDE()
    private void cancel(){
        
        DefaultTableModel model = (DefaultTableModel) recap.getModel();
        
       int n = recap.getRowCount();
       for (int i = n-1; i >=0;i--){
           model.removeRow(i);
       }
       
       lesArticle.clear();
       lesPrix.clear();
       cancelUneCmd.setEnabled(false);
       totalHT.setText("0.0");
       TVA.setText("0.0");
       totalTTC.setText("0.0");
}
       // Calcul le total
   private void Total(){

       
       
       int n = recap.getRowCount();
      // System.out.println(n);
       float TotalHT = 0f;
       float TotalTTC = 0f;
       float TotalTVA = 0f;
       
       //Calcul pour HT
       for(int i =0; i<n;i++){
         TotalHT += (float) recap.getModel().getValueAt(i,1);  
       }
       TotalTVA = TotalHT * 0.021f;
            
       TotalTTC = TotalHT + TotalTVA;
       
       //Pour les arrondis
       DecimalFormat df = new DecimalFormat();
       df.setMaximumFractionDigits(2);
       df.setMinimumFractionDigits ( 2 ) ;
       df.setDecimalSeparatorAlwaysShown (true) ; 
       
       totalHT.setText(String.valueOf(TotalHT));
       totalTTC.setText(String.valueOf(df.format(TotalTTC)));
       TVA.setText(String.valueOf(df.format(TotalTVA)));
   }
   
   //Vérifique les Recap sont rempli
   private void verifRecap(){
       int n = recap.getRowCount();
       
       //On vérifie contiens aucun element.
       if(n == 0){
           disableBtn();
       }
       //On fait rien
   }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Connexion = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jToggleButton1 = new javax.swing.JToggleButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        user = new javax.swing.JTextField();
        password = new javax.swing.JPasswordField();
        jLabel5 = new javax.swing.JLabel();
        Caisse = new javax.swing.JPanel();
        commande = new javax.swing.JScrollPane();
        recap = new javax.swing.JTable();
        description = new javax.swing.JTextField();
        prix = new javax.swing.JTextField();
        milkshake = new javax.swing.JButton();
        glace = new javax.swing.JButton();
        stepTwo = new javax.swing.JPanel();
        chocolat = new javax.swing.JButton();
        poire = new javax.swing.JButton();
        cafe = new javax.swing.JButton();
        framboise = new javax.swing.JButton();
        vanille = new javax.swing.JButton();
        ananas = new javax.swing.JButton();
        fraise = new javax.swing.JButton();
        rhum = new javax.swing.JButton();
        Valide = new javax.swing.JPanel();
        cancel = new javax.swing.JToggleButton();
        valid = new javax.swing.JButton();
        Total = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        totalHT = new javax.swing.JTextField();
        totalTTC = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        TVA = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        ValideCmd = new javax.swing.JPanel();
        cancelCmd = new javax.swing.JButton();
        validCmd = new javax.swing.JButton();
        info = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        cancelUneCmd = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        date = new javax.swing.JLabel();
        heures = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        sessionopen = new javax.swing.JLabel();
        TopArticle = new javax.swing.JPanel();
        Glace = new javax.swing.JToggleButton();
        Milkshake = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        Menu = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        gestionCompte = new javax.swing.JMenu();
        deconnexion = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        affichger = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Logiciel de caisse");

        Connexion.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("Connexion d'utilisateur");

        jToggleButton1.setText("Connexion");
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });

        jLabel2.setText("Nom d'utilisateur :");

        jLabel3.setText("Mot de passe:");

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/user.png"))); // NOI18N

        javax.swing.GroupLayout ConnexionLayout = new javax.swing.GroupLayout(Connexion);
        Connexion.setLayout(ConnexionLayout);
        ConnexionLayout.setHorizontalGroup(
            ConnexionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ConnexionLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(ConnexionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ConnexionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(user, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(295, 295, 295))
            .addGroup(ConnexionLayout.createSequentialGroup()
                .addGroup(ConnexionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ConnexionLayout.createSequentialGroup()
                        .addGap(332, 332, 332)
                        .addComponent(jLabel5))
                    .addGroup(ConnexionLayout.createSequentialGroup()
                        .addGap(207, 207, 207)
                        .addComponent(jLabel1))
                    .addGroup(ConnexionLayout.createSequentialGroup()
                        .addGap(390, 390, 390)
                        .addComponent(jToggleButton1)))
                .addContainerGap(264, Short.MAX_VALUE))
        );
        ConnexionLayout.setVerticalGroup(
            ConnexionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ConnexionLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(ConnexionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(user, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(ConnexionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToggleButton1)
                .addContainerGap(154, Short.MAX_VALUE))
        );

        recap.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Commande", "Prix €"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        commande.setViewportView(recap);

        description.setEditable(false);
        description.setBackground(new java.awt.Color(254, 254, 254));
        description.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        description.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                descriptionActionPerformed(evt);
            }
        });

        prix.setEditable(false);
        prix.setBackground(new java.awt.Color(254, 254, 254));
        prix.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        prix.setText("0.0");
        prix.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prixActionPerformed(evt);
            }
        });

        milkshake.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/milkshake.png"))); // NOI18N
        milkshake.setText("MILKSHAKE");
        milkshake.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                milkshakeActionPerformed(evt);
            }
        });

        glace.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/glace.png"))); // NOI18N
        glace.setText("GLACE");
        glace.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                glaceActionPerformed(evt);
            }
        });

        chocolat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/chocolate.png"))); // NOI18N
        chocolat.setText("Chocolat");
        chocolat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chocolatActionPerformed(evt);
            }
        });

        poire.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/poire.png"))); // NOI18N
        poire.setText("Poire");
        poire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                poireActionPerformed(evt);
            }
        });

        cafe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/cofee.png"))); // NOI18N
        cafe.setText("Café");
        cafe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cafeActionPerformed(evt);
            }
        });

        framboise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/framboise.png"))); // NOI18N
        framboise.setText("Framboise");
        framboise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                framboiseActionPerformed(evt);
            }
        });

        vanille.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/vanille.png"))); // NOI18N
        vanille.setText("Vanille");
        vanille.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vanilleActionPerformed(evt);
            }
        });

        ananas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/Ananas.png"))); // NOI18N
        ananas.setText("Ananas");
        ananas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ananasActionPerformed(evt);
            }
        });

        fraise.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/fraise.png"))); // NOI18N
        fraise.setText("Fraise");
        fraise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fraiseActionPerformed(evt);
            }
        });

        rhum.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/rhum.png"))); // NOI18N
        rhum.setText("Rhum charette");
        rhum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rhumActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout stepTwoLayout = new javax.swing.GroupLayout(stepTwo);
        stepTwo.setLayout(stepTwoLayout);
        stepTwoLayout.setHorizontalGroup(
            stepTwoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stepTwoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(stepTwoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(cafe, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chocolat, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                    .addComponent(poire, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ananas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(stepTwoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(stepTwoLayout.createSequentialGroup()
                        .addGroup(stepTwoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(framboise, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rhum, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 24, Short.MAX_VALUE))
                    .addGroup(stepTwoLayout.createSequentialGroup()
                        .addGroup(stepTwoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(vanille, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(fraise, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        stepTwoLayout.setVerticalGroup(
            stepTwoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stepTwoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(stepTwoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chocolat, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(vanille, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(stepTwoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ananas, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fraise, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(stepTwoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cafe, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(framboise, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(stepTwoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(poire, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rhum, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cancel.setBackground(new java.awt.Color(255, 51, 51));
        cancel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/refus.png"))); // NOI18N
        cancel.setText("ANNULER");
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });

        valid.setBackground(new java.awt.Color(102, 255, 102));
        valid.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/check.png"))); // NOI18N
        valid.setText("VALIDER");
        valid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ValideLayout = new javax.swing.GroupLayout(Valide);
        Valide.setLayout(ValideLayout);
        ValideLayout.setHorizontalGroup(
            ValideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ValideLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cancel, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(valid, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
                .addContainerGap())
        );
        ValideLayout.setVerticalGroup(
            ValideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, ValideLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ValideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(valid, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cancel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel6.setText("TOTAL HT");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("TOTAL TTC (2.10%)");

        totalHT.setEditable(false);
        totalHT.setBackground(new java.awt.Color(254, 254, 254));
        totalHT.setText("0.00");
        totalHT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totalHTActionPerformed(evt);
            }
        });

        totalTTC.setEditable(false);
        totalTTC.setBackground(new java.awt.Color(254, 254, 254));
        totalTTC.setText("0.00");

        jLabel8.setText("€");

        jLabel9.setText("€");

        jLabel10.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel10.setText("TVA");

        TVA.setEditable(false);
        TVA.setBackground(new java.awt.Color(254, 254, 254));
        TVA.setText("0.00");
        TVA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TVAActionPerformed(evt);
            }
        });

        jLabel13.setText("€");

        javax.swing.GroupLayout TotalLayout = new javax.swing.GroupLayout(Total);
        Total.setLayout(TotalLayout);
        TotalLayout.setHorizontalGroup(
            TotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TotalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel10)
                    .addComponent(jLabel7))
                .addGap(151, 151, 151)
                .addGroup(TotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(TotalLayout.createSequentialGroup()
                        .addComponent(totalTTC, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9))
                    .addGroup(TotalLayout.createSequentialGroup()
                        .addComponent(totalHT, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8))
                    .addGroup(TotalLayout.createSequentialGroup()
                        .addComponent(TVA, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel13)))
                .addContainerGap())
        );
        TotalLayout.setVerticalGroup(
            TotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TotalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(totalHT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(TotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TVA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(TotalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(totalTTC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        cancelCmd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/refus.png"))); // NOI18N
        cancelCmd.setText("ANNULER");
        cancelCmd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelCmdActionPerformed(evt);
            }
        });

        validCmd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/check.png"))); // NOI18N
        validCmd.setText("VALIDER");
        validCmd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validCmdActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout ValideCmdLayout = new javax.swing.GroupLayout(ValideCmd);
        ValideCmd.setLayout(ValideCmdLayout);
        ValideCmdLayout.setHorizontalGroup(
            ValideCmdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ValideCmdLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cancelCmd, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addComponent(validCmd, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
        );
        ValideCmdLayout.setVerticalGroup(
            ValideCmdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ValideCmdLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ValideCmdLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cancelCmd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(validCmd, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE))
                .addContainerGap())
        );

        jLabel11.setText("Date :");

        cancelUneCmd.setText("Annuler une commande");
        cancelUneCmd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelUneCmdActionPerformed(evt);
            }
        });

        jLabel12.setText("Heure :");

        date.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        date.setText("XX/XX/XXXX");

        heures.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        heures.setText("HH:MM:SS");

        jLabel4.setText("Session ouvert :");

        sessionopen.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        sessionopen.setText("XXXX.XXXX");

        javax.swing.GroupLayout infoLayout = new javax.swing.GroupLayout(info);
        info.setLayout(infoLayout);
        infoLayout.setHorizontalGroup(
            infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(infoLayout.createSequentialGroup()
                .addGroup(infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addGroup(infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(infoLayout.createSequentialGroup()
                            .addGap(48, 48, 48)
                            .addComponent(jLabel12))
                        .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sessionopen)
                    .addComponent(date)
                    .addComponent(heures))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addComponent(cancelUneCmd, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40))
        );
        infoLayout.setVerticalGroup(
            infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(infoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(infoLayout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addGroup(infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(date)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(heures))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(infoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(sessionopen)))
                    .addComponent(cancelUneCmd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout CaisseLayout = new javax.swing.GroupLayout(Caisse);
        Caisse.setLayout(CaisseLayout);
        CaisseLayout.setHorizontalGroup(
            CaisseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CaisseLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CaisseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CaisseLayout.createSequentialGroup()
                        .addComponent(description)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(prix, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(CaisseLayout.createSequentialGroup()
                        .addGroup(CaisseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(Total, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(commande, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(CaisseLayout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(Valide, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(CaisseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(CaisseLayout.createSequentialGroup()
                                .addGap(0, 12, Short.MAX_VALUE)
                                .addComponent(glace, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(milkshake, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6))
                            .addGroup(CaisseLayout.createSequentialGroup()
                                .addGroup(CaisseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(CaisseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(ValideCmd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(info, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(CaisseLayout.createSequentialGroup()
                                        .addGap(36, 36, 36)
                                        .addComponent(stepTwo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        CaisseLayout.setVerticalGroup(
            CaisseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CaisseLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CaisseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(description, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(prix, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(CaisseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CaisseLayout.createSequentialGroup()
                        .addComponent(commande, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Valide, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15))
                    .addGroup(CaisseLayout.createSequentialGroup()
                        .addGroup(CaisseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(milkshake, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(glace, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stepTwo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ValideCmd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(info, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        Glace.setText("Glace");
        Glace.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GlaceActionPerformed(evt);
            }
        });

        Milkshake.setText("Milkshake");
        Milkshake.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MilkshakeActionPerformed(evt);
            }
        });

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null}
            },
            new String [] {
                "Nom", "Nb"
            }
        ));
        jScrollPane1.setViewportView(table);

        javax.swing.GroupLayout TopArticleLayout = new javax.swing.GroupLayout(TopArticle);
        TopArticle.setLayout(TopArticleLayout);
        TopArticleLayout.setHorizontalGroup(
            TopArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TopArticleLayout.createSequentialGroup()
                .addGroup(TopArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(TopArticleLayout.createSequentialGroup()
                        .addGap(243, 243, 243)
                        .addComponent(Glace)
                        .addGap(131, 131, 131)
                        .addComponent(Milkshake))
                    .addGroup(TopArticleLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(333, Short.MAX_VALUE))
        );
        TopArticleLayout.setVerticalGroup(
            TopArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TopArticleLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(TopArticleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Glace)
                    .addComponent(Milkshake))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(103, Short.MAX_VALUE))
        );

        jMenu1.setText("Fichier");

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/code.png"))); // NOI18N
        jMenuItem1.setText("Code Source");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/refus.png"))); // NOI18N
        jMenuItem2.setText("Quitter");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        Menu.add(jMenu1);

        gestionCompte.setText("Gestion compte");

        deconnexion.setText("Déconnexion");
        deconnexion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deconnexionActionPerformed(evt);
            }
        });
        gestionCompte.add(deconnexion);

        Menu.add(gestionCompte);

        jMenu2.setText("Aide");

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/help.png"))); // NOI18N
        jMenuItem3.setText("A propos");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        Menu.add(jMenu2);

        jMenu3.setText("TopCommande");
        jMenu3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu3ActionPerformed(evt);
            }
        });

        affichger.setText("afficher");
        affichger.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                affichgerActionPerformed(evt);
            }
        });
        jMenu3.add(affichger);

        Menu.add(jMenu3);

        setJMenuBar(Menu);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Connexion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(Caisse, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(TopArticle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(Connexion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(Caisse, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(TopArticle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        try {
            String url = "https://gitlab.com/emma.t974/minilogicielcaisse";
            java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
        } catch (IOException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }

 
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        System.exit(0);     
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
       JOptionPane.showMessageDialog(this, "Auteur : EMMA TONY GEORGES\nVersion : 1.0\nPour l'épreuve E4\nLogiciel Pédagogie\nIcone prise sur : https://icon-icons.com");
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed

        String u = user.getText();
        String pwd = password.getText();
        
        session = new Vendeur(u,pwd);
          try {
            if(session.connexions() == true){
            pannelCnx(false);
            System.out.println(session.connexions());
            //lc = new LogicielCaisse(req);
            }
            else{
            JOptionPane.showMessageDialog(this, "Connexion refuser");
            }
            } catch (SQLException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
            }

    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void descriptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_descriptionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_descriptionActionPerformed

    private void totalHTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totalHTActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_totalHTActionPerformed

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelActionPerformed
      cancel();
      disableBtn();
      resetDescription();
    }//GEN-LAST:event_cancelActionPerformed

    private void glaceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_glaceActionPerformed

            enabledCmd();
            disableChoose();
            description("Glace");
        try {
            price = lesArticles.getPrice(1);
        } catch (SQLException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_glaceActionPerformed

    private void chocolatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chocolatActionPerformed
        enabledACmd();
        disableCmd();
        description("Chocolat");
    }//GEN-LAST:event_chocolatActionPerformed

    private void vanilleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vanilleActionPerformed
       enabledACmd();
       disableCmd();
        description("Vanille");
    }//GEN-LAST:event_vanilleActionPerformed

    private void ananasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ananasActionPerformed
       enabledACmd();
       disableCmd();
        description("Ananas");
    }//GEN-LAST:event_ananasActionPerformed

    private void fraiseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fraiseActionPerformed
        enabledACmd();
        disableCmd();
         description("Fraise");
    }//GEN-LAST:event_fraiseActionPerformed

    private void cafeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cafeActionPerformed
        enabledACmd();
        disableCmd();
        description("Café");
    }//GEN-LAST:event_cafeActionPerformed

    private void framboiseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_framboiseActionPerformed
         enabledACmd();
         disableCmd();
         description("Framboise");
    }//GEN-LAST:event_framboiseActionPerformed

    private void poireActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_poireActionPerformed
         enabledACmd();
         disableCmd();
         description("Poire");
    }//GEN-LAST:event_poireActionPerformed

    private void rhumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rhumActionPerformed
        enabledACmd();
        disableCmd();
        description("Rhum charette");
    }//GEN-LAST:event_rhumActionPerformed

    private void cancelCmdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelCmdActionPerformed
        disableBtn();
        //On reset la description
        resetDescription();
    }//GEN-LAST:event_cancelCmdActionPerformed

    private void milkshakeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_milkshakeActionPerformed
       enabledCmd();
       disableChoose();
       description("Milkshake");
       try {
            price = lesArticles.getPrice(2);
        } catch (SQLException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_milkshakeActionPerformed

    private void validCmdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_validCmdActionPerformed
        add();
        disableBtn();
        resetDescription();
        enabledAVCmd();
        
        cancelUneCmd.setEnabled(true);
        Total();
        
    }//GEN-LAST:event_validCmdActionPerformed

    private void prixActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prixActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_prixActionPerformed

    private void cancelUneCmdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelUneCmdActionPerformed
        delete();
        verifRecap();
    }//GEN-LAST:event_cancelUneCmdActionPerformed

    private void deconnexionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deconnexionActionPerformed
           session = new Vendeur();
           cancel();
           disableBtn();
           resetDescription();
           pannelCnx(true);
    }//GEN-LAST:event_deconnexionActionPerformed

    private void validActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_validActionPerformed
        try {
            lesArticles.ValideCommande(lesArticle, lesPrix, session);
            cancel();
        } catch (SQLException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            JOptionPane.showMessageDialog(this, "COMMANDE NUMERO : " + lesArticles.DernierCommandeAjout() + " AJOUTER !");
        } catch (SQLException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_validActionPerformed

    private void TVAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TVAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TVAActionPerformed

    private void GlaceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GlaceActionPerformed

        HashMap <String,Integer> tArticle = new HashMap<>();
        try {
            tArticle = lesArticles.getCommandePlusDemander("Glace");
        } catch (SQLException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        topCommande(tArticle);
        
    }//GEN-LAST:event_GlaceActionPerformed

    private void MilkshakeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MilkshakeActionPerformed
       HashMap <String,Integer> tArticle = new HashMap<>();
        try {
            tArticle = lesArticles.getCommandePlusDemander("Milkshake");
        } catch (SQLException ex) {
            Logger.getLogger(Interface.class.getName()).log(Level.SEVERE, null, ex);
        }
        topCommande(tArticle);
    }//GEN-LAST:event_MilkshakeActionPerformed

    private void jMenu3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu3ActionPerformed
   
    }//GEN-LAST:event_jMenu3ActionPerformed

    private void affichgerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_affichgerActionPerformed
       Connexion.setVisible(false);
       Caisse.setVisible(false);
       TopArticle.setVisible(true);
    }//GEN-LAST:event_affichgerActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interface().setVisible(true);
            }
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Caisse;
    private javax.swing.JPanel Connexion;
    private javax.swing.JToggleButton Glace;
    private javax.swing.JMenuBar Menu;
    private javax.swing.JButton Milkshake;
    private javax.swing.JTextField TVA;
    private javax.swing.JPanel TopArticle;
    private javax.swing.JPanel Total;
    private javax.swing.JPanel Valide;
    private javax.swing.JPanel ValideCmd;
    private javax.swing.JMenuItem affichger;
    private javax.swing.JButton ananas;
    private javax.swing.JButton cafe;
    private javax.swing.JToggleButton cancel;
    private javax.swing.JButton cancelCmd;
    private javax.swing.JButton cancelUneCmd;
    private javax.swing.JButton chocolat;
    private javax.swing.JScrollPane commande;
    private javax.swing.JLabel date;
    private javax.swing.JMenuItem deconnexion;
    private javax.swing.JTextField description;
    private javax.swing.JButton fraise;
    private javax.swing.JButton framboise;
    private javax.swing.JMenu gestionCompte;
    private javax.swing.JButton glace;
    private javax.swing.JLabel heures;
    private javax.swing.JPanel info;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JButton milkshake;
    private javax.swing.JPasswordField password;
    private javax.swing.JButton poire;
    private javax.swing.JTextField prix;
    private javax.swing.JTable recap;
    private javax.swing.JButton rhum;
    private javax.swing.JLabel sessionopen;
    private javax.swing.JPanel stepTwo;
    private javax.swing.JTable table;
    private javax.swing.JTextField totalHT;
    private javax.swing.JTextField totalTTC;
    private javax.swing.JTextField user;
    private javax.swing.JButton valid;
    private javax.swing.JButton validCmd;
    private javax.swing.JButton vanille;
    // End of variables declaration//GEN-END:variables
}
