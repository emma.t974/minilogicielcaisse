-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: mysql-emmat974.alwaysdata.net
-- Generation Time: May 31, 2019 at 08:09 AM
-- Server version: 10.2.22-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emmat974_caisse`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`id`, `name`, `price`) VALUES
(1, 'Glace', 2),
(2, 'Milkshake', 4);

-- --------------------------------------------------------

--
-- Table structure for table `commande`
--

CREATE TABLE `commande` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `heure` time NOT NULL,
  `idVendeur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commande`
--

INSERT INTO `commande` (`id`, `date`, `heure`, `idVendeur`) VALUES
(2, '2019-05-03', '08:41:45', 1),
(3, '2019-05-03', '08:42:40', 1),
(4, '2019-05-03', '08:44:41', 1),
(5, '2019-05-03', '08:45:22', 1),
(6, '2019-05-03', '08:45:29', 1),
(7, '2019-05-03', '08:54:22', 2),
(8, '2019-05-03', '08:55:11', 3),
(9, '2019-05-03', '08:55:17', 3),
(10, '2019-05-03', '08:55:27', 3),
(11, '2019-05-03', '08:56:35', 1),
(12, '2019-05-03', '08:19:19', 1),
(13, '2019-05-03', '08:55:46', 1),
(14, '2019-05-04', '08:11:43', 1),
(15, '2019-05-06', '06:28:59', 1),
(16, '2019-05-06', '06:43:54', 1),
(17, '2019-05-20', '07:36:58', 2),
(18, '2019-05-31', '08:08:20', 1);

--
-- Triggers `commande`
--
DELIMITER $$
CREATE TRIGGER `trig_histo_vendeur` AFTER INSERT ON `commande` FOR EACH ROW BEGIN
    INSERT INTO histo_vendeur
      (action, date, idCommande, idVendeur)
    VALUES
      ('insert', NOW(), new.id, new.idVendeur);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `contenu_commande`
--

CREATE TABLE `contenu_commande` (
  `id` int(11) NOT NULL,
  `idCommande` int(11) NOT NULL,
  `Article` varchar(60) NOT NULL,
  `Price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contenu_commande`
--

INSERT INTO `contenu_commande` (`id`, `idCommande`, `Article`, `Price`) VALUES
(1, 3, 'Glace Framboise ', 2),
(2, 3, 'Milkshake Poire ', 4),
(3, 3, 'Glace Rhum ', 2),
(4, 4, 'Glace Framboise ', 2),
(5, 4, 'Milkshake Poire ', 4),
(6, 4, 'Glace Rhum ', 2),
(7, 5, 'Glace Framboise ', 2),
(8, 5, 'Milkshake Poire ', 4),
(9, 5, 'Glace Ananas ', 2),
(10, 5, 'Milkshake Rhum ', 4),
(11, 5, 'Glace Rhum ', 2),
(12, 5, 'Milkshake Café ', 4),
(13, 6, 'Glace Ananas ', 2),
(14, 6, 'Milkshake Rhum ', 4),
(15, 7, 'Glace Framboise ', 2),
(16, 7, 'Milkshake Poire ', 4),
(17, 7, 'Glace Café ', 2),
(18, 8, 'Glace Framboise ', 2),
(19, 8, 'Milkshake Poire ', 4),
(20, 8, 'Glace Café ', 2),
(21, 9, 'Glace Ananas ', 2),
(22, 9, 'Milkshake Framboise ', 4),
(23, 10, 'Milkshake Rhum ', 4),
(24, 10, 'Glace Poire ', 2),
(25, 10, 'Milkshake Ananas ', 4),
(26, 11, 'Glace Framboise ', 2),
(27, 11, 'Milkshake Poire ', 4),
(28, 11, 'Milkshake Ananas ', 4),
(29, 12, 'Glace Fraise ', 2),
(30, 12, 'Milkshake Café ', 4),
(31, 12, 'Glace Rhum ', 2),
(32, 13, 'Glace Framboise ', 2),
(33, 13, 'Milkshake Café ', 4),
(34, 13, 'Glace Rhum ', 2),
(35, 13, 'Milkshake Poire ', 4),
(36, 14, 'Glace Framboise ', 2),
(37, 14, 'Milkshake Ananas ', 4),
(38, 14, 'Glace Rhum ', 2),
(39, 14, 'Milkshake Poire ', 4),
(40, 14, 'Glace Framboise ', 2),
(41, 15, 'Glace Framboise ', 2),
(42, 16, 'Milkshake Rhum ', 4),
(43, 17, 'Glace Rhum charette ', 2),
(44, 17, 'Milkshake Poire ', 4),
(45, 17, 'Glace Poire ', 2),
(46, 17, 'Milkshake Poire ', 4),
(47, 18, 'Glace Fraise ', 2),
(48, 18, 'Milkshake Poire ', 4);

-- --------------------------------------------------------

--
-- Table structure for table `histo_vendeur`
--

CREATE TABLE `histo_vendeur` (
  `id` int(11) NOT NULL,
  `action` enum('insert','update') DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `idCommande` int(11) NOT NULL DEFAULT 0,
  `idVendeur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `histo_vendeur`
--

INSERT INTO `histo_vendeur` (`id`, `action`, `date`, `idCommande`, `idVendeur`) VALUES
(1, 'insert', '2019-05-31 08:08:20', 18, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendeur`
--

CREATE TABLE `vendeur` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendeur`
--

INSERT INTO `vendeur` (`id`, `name`, `password`) VALUES
(1, 'tony.emma', '123456'),
(2, 'benjamin.turpin', '123456'),
(3, 'pierre.goubeaux', '123456');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idVendeur` (`idVendeur`);

--
-- Indexes for table `contenu_commande`
--
ALTER TABLE `contenu_commande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCommande` (`idCommande`);

--
-- Indexes for table `histo_vendeur`
--
ALTER TABLE `histo_vendeur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendeur`
--
ALTER TABLE `vendeur`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `contenu_commande`
--
ALTER TABLE `contenu_commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `histo_vendeur`
--
ALTER TABLE `histo_vendeur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendeur`
--
ALTER TABLE `vendeur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `commande_ibfk_1` FOREIGN KEY (`idVendeur`) REFERENCES `vendeur` (`id`);

--
-- Constraints for table `contenu_commande`
--
ALTER TABLE `contenu_commande`
  ADD CONSTRAINT `contenu_commande_ibfk_1` FOREIGN KEY (`idCommande`) REFERENCES `commande` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
